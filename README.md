# Avif Library
Manage the Avif conversion through a standalone library CAVIF-RS.

## Description
Define a new AvifProcessor Plugin "Cavif"  based on a CAVIF Rust library.
@see: https://github.com/kornelski/cavif-rs

## Requirement
The PHP user should have the right to execute the library on the server.

## Dependencies
- CAVIF-RS library included in this module,
- Avif module:  https://www.drupal.org/project/avif

## Installation
`composer require drupal/avif_lib`

Set the processor to "CAVIF RS" in AVIF module config:  
admin >> config >> media >> avif

@see https://www.drupal.org/project/avif

## Configuration
admin >> config >> media >> avif_lib
- set the library path

## Maintainers
* Mickaël Silvert - https://www.drupal.org/user/yepa
