<?php

namespace Drupal\avif_lib\Plugin\AvifProcessor;

use Drupal\avif_lib\Plugin\AvifProcessor\AvifShellOperationsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\avif\Plugin\AvifProcessorBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses the CAVIF binary to convert images.
 *
 * @AvifProcessor(
 *   id = "cavif",
 *   label = @Translation("CAVIF RS")
 * )
 */
class Cavif extends AvifProcessorBase implements ContainerFactoryPluginInterface {

  /**
   * The file system service.
   *
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The imageapi shell operation service.
   *
   * @var AvifShellOperationsInterface
   */
  protected $shellOperations;

  /**
   * Library path.
   *
   * @var string
   */
  protected $libPath;

  /**
   * Cavif constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param FileSystemInterface $file_system
   * @param AvifShellOperationsInterface $shell_operations
   */
  /**
   * Cavif constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param FileSystemInterface $file_system
   * @param AvifShellOperationsInterface $shell_operations
   * @param ConfigFactoryInterface $configFactory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $file_system, AvifShellOperationsInterface $shell_operations, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileSystem = $file_system;
    $this->shellOperations = $shell_operations;
    $this->libPath = $configFactory->get('avif_lib.settings')->get('path');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('avif_lib.shell_operations'),
      $container->get('config.factory')
    );
  }

  /**
   * Convert
   * @see https://github.com/kornelski/cavif-rs
   *
   * @param $image_uri
   * @param $quality
   * @param $destination
   * @return bool
   */
  public function convert($image_uri, $quality, $destination) {
    $cmd = $this->getFullPathToBinary();
    $arguments = [$this->sanitizeFilename($image_uri)];
    $options = ['--overwrite', '--quiet'];

    if (is_numeric($quality)) {
      $options[] = '--quality=' . escapeshellarg($quality);
    }

    return $this->execShellCommand($cmd, $options, $arguments);
  }

  /**
   * Execute a shell command on the local system.
   *
   * @param $command
   *   The command to execute.
   * @param $options
   *   An array of options for the command. This will not be escaped before executing.
   * @param $arguments
   *   An array of arguments for the command. These will be escaped.
   *
   * @return bool
   *   Returns TRUE if the command completed successfully, FALSE otherwise.
   */
  public function execShellCommand($command, $options, $arguments) {
    return $this->shellOperations->execShellCommand($command, $options, $arguments);
  }

  /**
   * Sanitize the given path for binary processors.
   *
   * @param $drupal_filepath
   *   The file path to sanitize.
   *
   * @return string
   *   The sanitized file path.
   */
  protected function sanitizeFilename($drupal_filepath) {
    return $this->fileSystem->realpath($drupal_filepath);
  }

  public function saveCommandStdoutToFile($cmd, $dst) {
    return $this->shellOperations->saveCommandStdoutToFile($cmd, $dst);
  }

  public function convertSaveToFile($command, $options, $arguments, $dst) {
    $cmd=$this->shellOperations->formatCommand($command, $options, $arguments);

    return $this->shellOperations->saveCommandStdoutToFile($cmd, $dst);
  }


  /**
   * @return string
   */
  public function getFullPathToBinary() {
    return DRUPAL_ROOT . $this->libPath;
  }
}
