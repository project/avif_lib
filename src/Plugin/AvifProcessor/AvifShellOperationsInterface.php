<?php

namespace Drupal\avif_lib\Plugin\AvifProcessor;

interface AvifShellOperationsInterface {
  public function findExecutablePath($executable = NULL);
  public function execShellCommand($command, $options, $arguments);
  public function saveCommandStdoutToFile($cmd, $dst);
}
