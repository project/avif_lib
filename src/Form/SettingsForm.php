<?php

namespace Drupal\avif_lib\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'avif_lib.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'avif_lib_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('avif_lib.settings');

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library path'),
      '#default_value' => $config->get('path'),
      '#description' => $this->t('Relative path from: ') . DRUPAL_ROOT,
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];
    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => '<a href="/admin/config/media/avif/settings">Check the AVIF config.</a>'
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('avif_lib.settings')
      ->set('path', (string)$form_state->getValue('path'))
      ->save();
  }

}
